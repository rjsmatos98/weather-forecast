import React, { useState } from 'react';
import { noop } from "lodash";
import { Props } from "./types";
import { Container, SearchInput, SearchButton, SearchLabel} from "./styled";

const Header = (props: Props) => {
    const [city, setCity] = useState(""); 
    return(
        <Container>
            <SearchLabel htmlFor="serch-input">Search by city</SearchLabel>
            <SearchInput type="text" value={city} onChange={(e) => {
                setCity(e.target.value)
            }} />
            <SearchButton onClick={() => props.onSearch(city)}>Search</SearchButton>
        </Container>
    )
}

Header.defaultProps ={
    onSearch: noop
}

export default Header;